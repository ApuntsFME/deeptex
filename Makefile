RED=\033[0;31m
NORMAL=\033[0m

DEBUG=1

ifeq ($(DEBUG), 0)
    OUTPUT:= &> /dev/null
else
    OUTPUT:=
endif

all: deeptex.pdf deeptex

deeptex.pdf: deeptex.tex
	@echo "Compiling DeepTeX documentation..."
	@if pdftex deeptex.tex ${OUTPUT}; then \
	    echo "Successfully generated source documentation."; \
	else \
	    echo -e "${RED}[ERROR]: Couldn't compile deeptex.tex${NORMAL}"; \
	    echo "This is probably due to an error in your TeX/LaTeX installation, please revise it before reporting a bug."; \
	    exit 2; \
	fi

deeptex.tex: deeptex.w
	@echo "Generating DeepTeX source documentation..."
	@if cweave deeptex.w${OUTPUT}; then \
		echo "Successfully generated source documentation."; \
	else \
		echo -e "${RED}[ERROR]: cweave not found${NORMAL}"; \
		echo "Couldn't generate source documentation because cweave program couldn't be found, this is probably caused by an error in your TeX/LaTeX installation, please revise it before reporting a bug."; \
		exit 2; \
	fi

deeptex.c: deeptex.w
	@echo "Generating DeepTeX C++ code..."
	@if ctangle deeptex.w${OUTPUT}; then \
		echo "Successfully generated DeepTeX C++ code."; \
	else \
		echo -e "${RED}[ERROR]: ctangle not found${NORMAL}"; \
		echo "Couldn't generate c++ code because ctangle program couldn't be found, this is probably caused by an error in your TeX/LaTeX installation, please revise it before reporting a bug."; \
		exit 2; \
	fi

deeptex: deeptex.c
	@echo "Compiling DeepTeX..."
	@g++ -o deeptex deeptex.c${OUTPUT}
	@echo "DeepTeX compiled successfully"

.PHONY: clean all install

clean:
	@rm -f deeptex.c deeptex deeptex.tex deeptex.pdf deeptex.toc deeptex.idx deeptex.log deeptex.scn texput.log
	@echo "Directory cleaned"

install: deeptex
	@echo "Not implemented yet"
