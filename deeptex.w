% Copyright ApuntsFME, licensed under GPL v3.

@q Note, this IS NOT the source code of DeepTeX as it is intended        @>
@q to be read, in order to obtain the source code please run             @>
@q     cweave deeptex.w                                                  @>
@q And then run                                                          @>
@q     pdftex deeptex.tex                                                @>
@q to obtain a file called 'deeptex.pdf' which will be your source code. @>

@q To obtain a runnable program from this file, run                      @>
@q     ctangle deeptex.w && g++ -o deeptex deeptex.c                     @>

% TODO define propper LaTeX macro
\def\LaTeX{L\kern -0.3em\lower 0.3ex \hbox{A}\kern -0.15em\TeX}

@* Introduction.
This is a program in order to format common \LaTeX code. The motivation for this program comes from
a shared \LaTeX repository where notes on some university courses where posted. Every one of us
people working on that repository would have a unique way of writting the code and thus the
final code is very messy and quite hard to read. Therefore, in an attempt to standarize \LaTeX
(and \TeX) code, we made this program that formats \TeX/\LaTeX source files.

Although we agreed on this standars, many of us think is not a practic way of writting code and
refuse to code in this style. So to solve this issue, we created this program. This program wil
{\bf not} change the output of a \LaTeX/\TeX source file (and by any means make the output
prettier) but it will make de source code a lot more readable in our opinion.

The actual standars this program follows will be explained as we implement them, but you can take
a look at the Resumé in the last page.

@* Main algorithm.
Here's how we envision the main algorithm, we will be defining each of
the parts specified here later.
@c
int main(int argc, char* argv[]) {
	@<tokenization@>
}

@* Tokenization. The tokenization consist of mainly two parts, token creation and token definition, each
of this is explained in following sections.

@<tokenization@>=
@<token definition@>
@<token creation@>

@ Here we describe the basic token structure.

@ Here's how we define tokens.
@<token definition@>=
int a = 0;

@ And here's how we create them.
@<token creation@>=
int b = 4;
